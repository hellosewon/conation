# conation
Cryptocurrency donation platform for online broadcasters frontend (React/Javascript)

#### Conation
![image info](https://bytebucket.org/hellosewon/portfolio/raw/2cc1a50f93e447d307223cbc035f504d84b4d0eb/portfolio_preview/3_0_conation_project_overview.PNG)
#
![image info](https://bytebucket.org/hellosewon/portfolio/raw/2cc1a50f93e447d307223cbc035f504d84b4d0eb/portfolio_preview/3_1_conation.PNG)
#
![image info](https://bytebucket.org/hellosewon/portfolio/raw/2cc1a50f93e447d307223cbc035f504d84b4d0eb/portfolio_preview/3_2_conation.PNG)
#
![image info](https://bytebucket.org/hellosewon/portfolio/raw/2cc1a50f93e447d307223cbc035f504d84b4d0eb/portfolio_preview/3_3_conation.PNG)
#


# README #

This README document outlines the steps necessary to get conation application up and running.




### What is this repository for? ###

* Quick summary
Conation is a donation platform for streamers/broadcasters which utilizes Ethereum blockchain. This project employes React.js, Truffle and Geth.

### How do I get set up? ###

* Summary of set up
Install Node.js and npm
Install geth and sync the blockchain

* Configuration
# On Ubuntu 16.04 LTS (http://helloshark.works)


### 0. Install Node.js and npm ###
```
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```

`➜  ~ node -v`
v8.9.4
`➜  ~ npm -v`
5.6.0


### 1. Install geth and sync the blockchain (Can be another server) ###
```
sudo apt-get install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install ethereum
```

```
# Install Go 1.9
# Add go to PATH
git clone https://github.com/ethereum/go-ethereum.git
cd go-ethereum
git reset --hard 4bb3c89  # Roll back geth to 1.7.3 (from 1.8.0)
make geth
geth version  # 1.7.3
```

### 2. Install the Truffle Framework ###
sudo npm install -g truffle
mkdir conation
cd conation
truffle unbox react  // Make New Truffle Project or Pull Project from Git

### Running Geth RPC Server ###
```
geth --rinkeby --rpc --rpcapi db,eth,net,web3,personal --cache=1024  --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*"
// --rpcaddr 127.0.0.1 --prccorsdomain "helloshark.works"

eth.syncing
web3.eth.blockNumber
```


### Creating Account ###
```
geth attach ipc:$HOME/.ethereum/rinkeby/geth.ipc
> personal.newAccount("bird-nt")
"0xcb098ad7f17866e1b1dd7e171acc018fb3c5ae84"
> web3.personal.newAccount('bird-nt2')
"0xefd2ae502f3cf64b009634e23301acb620c53f62"
```


### Account List ###
```
geth --rinkeby account list
Account #0: {cb098ad7f17866e1b1dd7e171acc018fb3c5ae84} keystore:///home/conation/.ethereum/rinkeby/keystore/UTC--2018-02-17T05-41-32.067408675Z--cb098ad7f17866e1b1dd7e171acc018fb3c5ae84
Account #1: {efd2ae502f3cf64b009634e23301acb620c53f62} keystore:///home/conation/.ethereum/rinkeby/keystore/UTC--2018-02-17T15-26-06.266469252Z--efd2ae502f3cf64b009634e23301acb620c53f62
```


### Getting Balance ###
```
web3.fromWei(web3.eth.getBalance('0xcb098ad7f17866e1b1dd7e171acc018fb3c5ae84'), 'ether')
web3.fromWei(web3.eth.getBalance('0xefd2ae502f3cf64b009634e23301acb620c53f62'), 'ether')
```


### Unlocking Account for 15000 seconds. ###
```
web3.personal.unlockAccount('0xcb098ad7f17866e1b1dd7e171acc018fb3c5ae84', 'bird-nt', 15000)
web3.personal.unlockAccount('0xefd2ae502f3cf64b009634e23301acb620c53f62', 'bird-nt2', 15000)
```

### Sending Ether (May be use JWT Token for one time use)
### Checking if geth is connected
### Checking "from" address (both in geth and conation)
### Checking "to" address (both in geth and conation)
### Showing wallet info using Conation DB
### Checking if "from" address is unlocked
### Unlocking the account for x seconds
### Performing transaction
### Saving inforation to Conation DB
### Locking Account

# Performing Transactions
```
web3.eth.sendTransaction(
	{
		from: '0xefd2ae502f3cf64b009634e23301acb620c53f62',
		to: '0xcb098ad7f17866e1b1dd7e171acc018fb3c5ae84',
		value: web3.toWei(0.488869504, 'ether'),
	},
	function(err, transactionHash) {
		if (!err) console.log(transactionHash);
		else console.log(err);
	}
);
```


# Estimating Gas
```
var gas_price = web3.eth.gasPrice

var gas_amount = web3.eth.estimateGas({
	from: '0xefd2ae502f3cf64b009634e23301acb620c53f62',
	to: '0xcb098ad7f17866e1b1dd7e171acc018fb3c5ae84',
	value: web3.toWei(1, 'ether'),
});

web3.eth.sendTransaction(
	{
		from: '0xefd2ae502f3cf64b009634e23301acb620c53f62',
		to: '0xcb098ad7f17866e1b1dd7e171acc018fb3c5ae84',
		value: web3.toWei(1, 'ether'),
		gas: gas_amount,
		gasPrice: gas_price,
	},
	function(err, transactionHash) {
		if (!err) console.log(transactionHash);
		else console.log(err);
	}
);
```

#TODO: Restore account usinng Keystore
#TODO: Auto-backup keystore

* Dependencies
See package.json

* Database configuration
Uses Firestore

* How to run tests
npm run start
http://localhost:3000

* Deployment instructions
Configure Firestore Database and Authentication in Firebase
npm build
Upload the static files to firebase


### Who do I talk to? ###

* Repo owner or admin
Created Date: 2018-02-16
Created By: Shark
* Other community or team contact
hellosewon@gmail.com


