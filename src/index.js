import React from 'react'
import ReactDOM from 'react-dom'
import { Switch, BrowserRouter, Route } from 'react-router-dom'
import PropTypes from 'prop-types'



import fire from './utils/fire'
import Home from './js/pages/Home'
import Streamers from './js/pages/Streamers'
import Comarket from './js/pages/Comarket'
import Wallets from './js/pages/Wallets'
// import SimpleCona from './js/pages/SimpleCona'
import Whitepaper from './js/pages/Whitepaper'
import Signin from './js/pages/Signin'
import Signup from './js/pages/Signup'
import Policy_Privacy from './js/pages/Policy/Privacy'
import Policy_Terms from './js/pages/Policy/Terms'
import My from './js/pages/My'
import Donate from './js/pages/Donate'
import OverlayTemp from './js/pages/OverlayTemp'


import './index.css'
require('./utils/myIcons')


const app_root = document.getElementById('root')

class ContextProvider extends React.Component {

	// TODO: USER REDUX
	static childContextTypes = {
		currentUser: PropTypes.object,
		title: PropTypes.string,
		// db: PropTypes.object,
	}

	constructor(props) {
		super(props);
		this.state = {
			title: "CONATION.IO",
			currentUser: undefined,
		}
	}

	getChildContext() {
		return {
			currentUser: this.state.currentUser,
			title: this.state.title,
		}
	}

	componentWillMount() {

		fire.auth().onAuthStateChanged((user) => {
			if (user) {
				// User is signed in.
				// alert(JSON.stringify(user))
				this.setState({ currentUser: user }, () => {
					// alert("Index User")
					// console.log(this.state.currentUser)
				})
			} else {
				// No user is signed in (null).
				this.setState({ currentUser: user }, () => {
					// alert("Index No User")
					// console.log(this.state.currentUser)
				})
			}
		})
	}

	render() {
		return(
<BrowserRouter>
	<Switch>
		<Route exact path="/" name="home" component={Home} />
		<Route exact path="/streamers" name="cona" component={Streamers} />
		<Route exact path="/comarket" name="cona" component={Comarket} />
		{/*<Route exact path="/cona" name="cona" component={SimpleCona} />*/}
		<Route exact path="/wallets" name="wallet" component={Wallets} />
		<Route exact path="/whitepaper" name="whitepaper" component={Whitepaper} />
		<Route exact path="/signin" name="signin" component={Signin} />
		<Route exact path="/signup" name="signup" component={Signup} />
		<Route exact path="/policy/privacy" name="policy_privacy" component={Policy_Privacy} />
		<Route exact path="/policy/terms" name="policy_terms" component={Policy_Terms} />
		<Route exact path="/my" name="my" component={My} />
		<Route exact path="/donate" name="donate" component={Donate} />
		<Route exact path="/ot" name="ot" component={OverlayTemp} />

		<Route render={() => <h1>Page not found</h1>} />
	</Switch>
</BrowserRouter>
		)
	}
}

ReactDOM.render(<ContextProvider />, app_root);
