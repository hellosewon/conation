import React from "react"



import '../../css/components/CardFeaturedStreamer.css'



const defaultUserIcon = require('../../img/profile-user.svg');
export default class CardFeaturedStreamer extends React.Component {

	get_twitchLink() {
		return "https://www.twitch.tv/" + this.props.userInfo.connected_accounts.twitch.id
	}

	get_youtubeLink() {
		return "https://www.youtube.com/channel/" + this.props.userInfo.connected_accounts.youtube.id
	}

	get_thumbnailURL() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].thumbnail
		if (data)
			return data
		else
			return defaultUserIcon
	}

	get_displayName() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].name
		if (data)
			return data
		else
			return "(no name)"
	}

	get_banner() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].banner
		if (data)
			return "url(" + data + ")"
		// else
		// 	return "#35404f"  // CONATION PRIMARY COLOR
	}

	get_description() {
		var main_account = this.props.userInfo.main_account
		var streamerInfo = this.props.userInfo.connected_accounts[main_account]
		if (main_account && streamerInfo) {
			var description = streamerInfo.description
			if (description)
				return description
			else
				return "(no description)"
		}
		else
			return "-"
	}

	render() {
		return (



<div className="col-sm-6 col-xl-4 CardFeaturedStreamer">
	<div className="banner" style={{ background: this.get_banner() }}></div>
	<div className="card-content text-white w-100">
		<div className="little-gutters">
			<div className="col-12">
				<div className="card-body">
					<div>
						<img className="rounded-circle mr-2" style={{width: 60, height: 60}}
							src={this.get_thumbnailURL()}
							alt="Connected Account" />
						<h2 className="card-title font-raleway font-weight-bold d-inline">{this.get_displayName()}</h2>
					</div>
					<p className="card-text">{this.get_description()}</p>
				</div>
			</div>
		</div>
	</div>
	<div className="spacer10"></div>
</div>



		)
	}
}