import React, { Component } from 'react'
import PropTypes from 'prop-types'



import fetch from 'node-fetch'
import fire from '../../../utils/fire'
import '../../../css/components/My/CardAccountInfo.css'



import gapi from '../../../utils/googleapi.js'
const defaultUserIcon = require('../../../img/profile-user.svg')
export default class CardAccountInfo extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	// constructor(props) {
	// 	super(props)

	// 	this.state = {
	// 		loadedAccounts: {},
	// 	}
	// }

	handleVerifyEmailClick = (e) => {
		var actionCodeSettings = {
			// url: 'https://www.example.com/cart?email=user@example.com&cartId=123',
		}
		this.context.currentUser.sendEmailVerification(actionCodeSettings)
		.then(function() {
			// Verification email sent.
			alert("Verification email sent.")
		})
		.catch(function(error) {
			// Error occurred. Inspect error.code.
			alert(error)
		})
	}

	handlePasswordResetClick = (e) => {
		var actionCodeSettings = {
			// url: 'https://www.example.com/?email=user@example.com',
		}
		var email = this.context.currentUser.email
		fire.auth().sendPasswordResetEmail(email, actionCodeSettings)
		.then(function() {
			alert("Password reset email sent.")
		})
		.catch(function(error) {
			// Error occurred. Inspect error.code.
			alert(error)
		})
	}

	render_twitchInfo() {
		if (this.props.userInfo)
			return <TwitchAccount
					checked={this.props.userInfo.main_account === 'twitch'}
					onCheckClick={this.onAccountCheckClick}
					channelInfo={this.props.userInfo.connected_accounts.twitch}
					onLoadChannel={this.updateUserProfile} />
		else
			return <li className="list-group-item disabled">Loading user information...</li>
	}

	render_youtubeInfo() {
		if (this.props.userInfo)
			return <YoutubeAccount
					checked={this.props.userInfo.main_account === 'youtube'}
					onCheckClick={this.onAccountCheckClick}
					channelInfo={this.props.userInfo.connected_accounts.youtube}
					onLoadChannel={this.updateUserProfile} />
		else
			return <li className="list-group-item disabled">Loading user information...</li>
	}

	updateUserProfile = (provider, channelInfo) => {
		let num_ca = Object.keys(this.props.userInfo.connected_accounts).length

		// If no connected accounts, main_account = null
		if (num_ca === 0) {
			this.updateMainAccount(null)
			return 
		}

		// // If connected_account is 1, always set it as a main_account
		if (num_ca === 1 && this.props.userInfo.connected_accounts[provider]) {
			this.updateMainAccount(provider)
		}
	}

	onAccountCheckClick = (provider) => {
		if (provider.toLowerCase() !== this.props.userInfo.main_account
			&& confirm(`Use ${provider} account as main?`))
			this.updateMainAccount(provider)
	}

	updateMainAccount(provider) {
		if (provider !== null)
			provider = provider.toLowerCase()
		if (this.props.userInfo.main_account === provider)
			return

		var db = fire.firestore()
		var userRef = db.collection('users').doc(this.context.currentUser.uid)
		return userRef.update({
			"main_account": provider
		}).then(() => {
			console.log("main_account successfully updated!")
			// Warning: main_account is not updated from DB
		}).catch(function(error) {
			// The document probably doesn't exist.
			console.error("Error updating main_account: ", error)
		})
	}

	get_thumbnailURL() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].thumbnail
		if (data)
			return data
		else
			return defaultUserIcon
	}

	get_displayName() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].name
		if (data)
			return data
		else
			return "(no name)"
	}

	get_banner() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].banner
		if (data)
			return "url(" + data + ")"
		// else
		// 	return "#35404f"  // CONATION PRIMARY COLOR
	}

	render() {
		return (



<div className="CardAccountInfo">
	<div className="card border rounded">

		<div className="card-header-x">
			<div className="banner border-bottom" style={{background: this.get_banner()}}></div>
			<div className="avatar">
				<img className="border-white bg-light" alt="" src={this.get_thumbnailURL()} />
			</div>
			<div className="info">
				<h5 className="title font-raleway">{this.get_displayName()}</h5>
			</div>
		</div>

		<div className="card-body pt-1">
			{/* Email */}
			<small><strong>EMAIL</strong></small>
			{this.context.currentUser.emailVerified ? (
				<span className="float-right m-0 p-0 text-muted" style={{fontSize: '0.8203125rem'}}>(verified)</span>
			) : (
				<button className="btn btn-link btn-sm float-right m-0 p-0"
					type="button" onClick={this.handleVerifyEmailClick}>verify email</button>
			)}
			<input type="text" className="form-control"
				value={this.context.currentUser.email} readOnly disabled />
			
			{/* Password */}
			<div className="spacer10"></div>
			<small><strong>PASSWORD</strong></small>
			<button className="btn btn-link btn-sm float-right m-0 p-0"
				type="button" onClick={this.handlePasswordResetClick}>reset password</button>
	
			<input type="text" className="form-control text-muted"
				value="(encrypted)" readOnly disabled />
			<div className="spacer15"></div>
	{/* Phone 
			<div className="spacer10"></div>
			<small><strong>PHONE</strong></small>
			<button className="btn btn-link btn-sm float-right m-0 p-0" type="button">verify phone</button>
			<input type="text" className="form-control"
				value=""/>
	*/}

	{/*
	<p className="card-text">
		UID: {JSON.stringify(this.context.currentUser.uid)}<br/>

		DISPLAY_NAME: {JSON.stringify(this.context.currentUser.displayName)}<br/>
		PHOTO_URL: {JSON.stringify(this.context.currentUser.photoURL)}<br/>
		{JSON.stringify(this.props.userInfo)}<br/>
		{Number(60.00000000000001)}<br/>
		{Number(60.000000000000001)}<br/>

		EMAIL: {JSON.stringify(this.context.currentUser.email)}<br/>
		EMAIL_VERIFIED: {JSON.stringify(this.context.currentUser.emailVerified)}<br/>
		Password [encrypted] [reset]<br/>
		PHONE_NUMBER: {JSON.stringify(this.context.currentUser.phoneNumber)}<br/>
		LAST_LOGIN_AT: {JSON.stringify(this.context.currentUser.metadata.lastSignInTime)}<br/>
		CREATED_AT: {JSON.stringify(this.context.currentUser.metadata.creationTime)}<br/>

		JSON.stringify(this.context.currentUser.providerData)}
	</p>
	*/}
		</div>
		<ul className="list-group list-group-flush">
			{this.render_twitchInfo()}
			{this.render_youtubeInfo()}
		</ul>
	</div>
</div>



		)
	}
}






var _isEqual = require('lodash/isEqual')
var _isEmpty = require('lodash/isEmpty')
/*=============================================>>>>>
= Twitch Account =
===============================================>>>>>*/
class TwitchAccount extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			CLIENT_ID: '9uxg7odhco9x46ce7cybtvqw6bx2wx',
			hash: this.parseHash(),
			channelInfo: undefined,
		}
	}

	parseHash() {
		const data = location.hash
		location.hash = ''  // For IE < 10 Support
		history.replaceState({}, null, "/my")

		const qs = require('query-string')
		const parsedHash = qs.parse(data)
		return parsedHash
	}

	componentDidMount() {
		this.setState({ channelInfo: this.props.channelInfo }, () => {
			this.initTwitchStatus()
		})
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.channelInfo !== this.state.channelInfo) {
			this.setState({ channelInfo: nextProps.channelInfo }, () => {
				this.initTwitchStatus()
			})
		}
	}

	initTwitchStatus() {
		this.setChannelInfo()
		if (this.getAccessToken())
			this.getChannelInfo(this.getAccessToken())
	}

	setChannelInfo() {
		if (this.state.channelInfo)
			var channelId = this.state.channelInfo.id
		if (channelId) {
			const url = 'https://api.twitch.tv/helix/users'
						+ '?login=' + channelId
			fetch(url, { 
				cache: 'no-store',
				headers: {'Client-ID': this.state.CLIENT_ID},
				method: 'GET',
			}).then((res) => {
				return res.json()
			}).then((json) => {
				// console.log(json)
				var channel = json.data[0]
				var channelInfo = {
					id: channel.login,
					name: channel.display_name,
					description: channel.description,
					thumbnail: channel.profile_image_url,
					banner: channel.offline_image_url,
				}
				this.setChannelState(channelInfo)
				// Save Channel Name in Search Keywords
			})
		} else {
			this.setChannelState({})
		}
	}

	setChannelState(channelInfo) {
		this.setState({channelInfo: channelInfo})
		this.updateChannelInfo(channelInfo)
		this.props.onLoadChannel("twitch", channelInfo)
	}

	handleConnectTwitchClick = (e) => {
		if (this.props.channelInfo && this.props.channelInfo.id)
			this.disconnectTwitch()
		else
			this.connectTwitch()
	}

	connectTwitch() {
		if (!this.isUpdateChannelInfoValid())
			return

		const url = 'https://id.twitch.tv/oauth2/authorize'
					+ '?client_id=' + this.state.CLIENT_ID
					+ '&redirect_uri=http://localhost:3000/my'
					+ '&response_type=token'
					+ '&scope=user_read'
					+ '&nonce=nonceTest'
					+ '&state=stateTest'

		window.location.href = url
	}

	disconnectTwitch() {
		if (confirm('Disconnect twitch account?')) {

			const firebase = require("firebase")
			// Required for side-effects
			require("firebase/firestore")


			var db = fire.firestore()
			var userRef = db.collection('users').doc(this.context.currentUser.uid)

			return userRef.update({
				"connected_accounts.twitch": firebase.firestore.FieldValue.delete(),
				}).then(() => {
					console.log("connected_accounts.twitch successfully deleted!")
				}).catch(function(error) {
					// The document probably doesn't exist.
					console.error("Error deleting connected_accounts.twitch: ", error)
				})
		}
	}

	getAccessToken() {
		return this.state.hash.access_token
	}

	revokeAccessToken(accessToken) {
		const url = 'https://id.twitch.tv/oauth2/revoke'
					+ '?client_id=' + this.state.CLIENT_ID
					+ '&token=' + accessToken

		fetch(url, { 
			cache: 'no-store',
			method: 'POST',
		}).then((res) => {
			if (res.status === 200)
				console.log("Twitch access_token revoked.")
			else
				console.log("Twith access_token failed to revoke.")
		})
	}

	getChannelInfo(accessToken) {
		fetch('https://api.twitch.tv/helix/users', { 
			cache: 'no-store',
			method: 'GET',
			// body:    JSON.stringify(body),
			headers: { 'Authorization': "Bearer " + accessToken },
		}).then((res) => {
			this.setState({ hash: {} })
			this.revokeAccessToken(accessToken)
			return res.json()
		}).then((json) => {
			// console.log(json)
			var channel = json.data[0]
			var channelInfo = {
				id: channel.login,
				name: channel.display_name,
				description: channel.description,
				thumbnail: channel.profile_image_url,
				banner: channel.offline_image_url,
			}
			this.saveChannelInfo(channelInfo)
		})
	}

	isDuplicate(channelId, callback) {
		var db = fire.firestore()
		var userRef = db.collection("users")

		userRef
		.where("connected_accounts.twitch.id", "==", channelId)
		.limit(1)
		.get().then((docs) => {
			let is_duplicate = docs.size > 0
				callback(is_duplicate)
		})
		.catch(function(error) {
			console.log("Error getting twitch isDuplicate: ", error)
		})
	}

	isUpdateChannelInfoValid() {
		if (!this.context.currentUser.emailVerified) {
			alert("Verify your email before connecting external accounts.")
			return false
		}

		return true
	}

	updateChannelInfo(channelInfo) {
		if (!channelInfo || _isEmpty(channelInfo))
			return
		if (!this.isUpdateChannelInfoValid())
			return

		let doUpdate = !_isEmpty(channelInfo) && !_isEqual(this.props.channelInfo, channelInfo)

		if (doUpdate) {
			var db = fire.firestore()
			var userRef = db.collection('users').doc(this.context.currentUser.uid)
			return userRef.update({
				"connected_accounts.twitch": channelInfo,
			}).then(() => {
				console.log("Twitch channelInfo successfully updated!")
			}).catch(function(error) {
				// The document probably doesn't exist.
				console.error("Error updating twitch channelInfo: ", error)
			})
		}

	}

	saveChannelInfo(channelInfo) {
		if (!this.isUpdateChannelInfoValid())
			return

		this.isDuplicate(channelInfo.id, (is_duplicate) => {
			if (is_duplicate) {
				alert("The twitch channel is already registered in conation.io")
			} else {
				var db = fire.firestore()
				var userRef = db.collection('users').doc(this.context.currentUser.uid)
				return userRef.update({
					"connected_accounts.twitch": channelInfo,
				}).then(() => {
					console.log("Twitch channelInfo successfully saved!")
				}).catch(function(error) {
					// The document probably doesn't exist.
					console.error("Error saving twitch channelInfo: ", error)
				})
			}
		})
	}

	render() {
		return (



			<AccountRow channelInfo={this.state.channelInfo}
				provider="Twitch" 
				checked={this.props.checked}
				onCheckClick={this.props.onCheckClick}
				handleConnectClick={this.handleConnectTwitchClick} />



)
	}
}
/*= End of Twitch Account =*/
/*=============================================<<<<<*/





/*=============================================>>>>>
= Youtube Account =
===============================================>>>>>*/
class YoutubeAccount extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			channelInfo: undefined
		}
	}

	componentDidMount() {
		this.setState({ channelInfo: this.props.channelInfo }, () => {
			this.initYoutubeStatus()
		})
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.channelInfo !== this.state.channelInfo) {
			this.setState({ channelInfo: nextProps.channelInfo }, () => {
				this.initYoutubeStatus()
			})
		}
	}

	initYoutubeStatus() {
		this.setChannelInfo()
	}

	setChannelInfo() {
		if (this.state.channelInfo)
			var channelId = this.state.channelInfo.id
		if (channelId) {
			let clientConfig = {
				apiKey: 'AIzaSyAzXHGRhRKht-6jRfVZOl9dYmTkc3_N5h8',
				discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest'],
			}

			gapi.load('client', () => {
				gapi.client.init(clientConfig).then(() => {
					if (channelId) {
						// Get Channel Info
						gapi.client.youtube.channels.list({
							'part': 'snippet,brandingSettings',  // ,contentDetails,statistics
							'id': [channelId],
						}).then((response) => {
							var channel = response.result.items[0]
							// alert(JSON.stringify(channel))
							var channelInfo = {
								id: channelId,
								name: channel.snippet.title,
								description: channel.snippet.description,
								thumbnail: channel.snippet.thumbnails.medium.url,
								banner: channel.brandingSettings.image.bannerImageUrl,
							}
							this.setChannelState(channelInfo)
							// Save Channel Name in Search Keywords
						})
					}
				})
			})
		} else {
			this.setChannelState({})
		}
	}

	setChannelState(channelInfo) {
		this.setState({channelInfo: channelInfo})
		this.updateChannelInfo(channelInfo)
		this.props.onLoadChannel("youtube", channelInfo)
	}

	handleConnectYoutubeClick = (e) => {
		if (this.props.channelInfo && this.props.channelInfo.id)
			this.disconnectYoutube()
		else
			this.connectYoutube()
	}

	connectYoutube() {
		if (!this.isUpdateChannelInfoValid())
			return

		// Load the API's client and auth2 modules.
		// Call the initClient function after the modules load.
		gapi.load('client', () => {
			this.getAccessToken((accessToken) => {
				if (accessToken) {
					this.getChannelInfo(accessToken)
				}
			})
		})
	}

	disconnectYoutube() {
		if (confirm('Disconnect youtube account?')) {

			const firebase = require("firebase")
			// Required for side-effects
			require("firebase/firestore")


			var db = fire.firestore()
			var userRef = db.collection('users').doc(this.context.currentUser.uid)

			return userRef.update({
				"connected_accounts.youtube": firebase.firestore.FieldValue.delete(),
				}).then(() => {
					console.log("connected_accounts.youtube successfully deleted!")
				}).catch(function(error) {
					// The document probably doesn't exist.
					console.error("Error deleting connected_accounts.youtube: ", error)
				})
		}
	}

	getAccessToken(callback) {
		let authConfig = {
			client_id: '876748981877-2j9avdn0i8j2a4e6j26p0rgjto5401ua.apps.googleusercontent.com',
			prompt: 'select_account',
			scope: 'https://www.googleapis.com/auth/youtube.readonly',
			response_type: 'permission',  //id_token code
		}

		gapi.auth2.authorize(authConfig, (response) => {
			if (response.error) {
				console.error(response.error)
				callback(null)
				return
			}
			callback(response.access_token)
		})
	}

	getChannelInfo(accessToken) {
		fetch('https://content.googleapis.com/youtube/v3/channels?mine=true&part=snippet,brandingSettings', { 
			cache: 'no-store',
			method: 'GET',
			// body:    JSON.stringify(body),
			headers: { 'Authorization': "Bearer " + accessToken },
		}).then((res) => {
			return res.json()
		}).then((json) => {
			// console.log(json)
			var channel = json.items[0]
			var banner = channel.brandingSettings.image.bannerMobleImageUrl
			if (!banner)
				banner = channel.brandingSettings.image.bannerImageUrl
			var channelInfo = {
				id: channel.id,
				name: channel.snippet.title,
				description: channel.snippet.description,
				thumbnail: channel.snippet.thumbnails.medium.url,
				banner: banner,
			}
			this.saveChannelInfo(channelInfo)
		})
	}

	isDuplicate(channelId, callback) {
		var db = fire.firestore()
		var userRef = db.collection("users")

		userRef
		.where("connected_accounts.youtube.id", "==", channelId)
		.limit(1)
		.get().then((docs) => {
			let is_duplicate = docs.size > 0
				callback(is_duplicate)
		})
		.catch(function(error) {
			console.log("Error getting youtube isDuplicate: ", error)
		})
	}

	isUpdateChannelInfoValid() {
		if (!this.context.currentUser.emailVerified) {
			alert("Verify your email before connecting external accounts.")
			return false
		}

		return true
	}

	updateChannelInfo(channelInfo) {
		if (!channelInfo || _isEmpty(channelInfo))
			return
		if (!this.isUpdateChannelInfoValid())
			return

		let doUpdate = !_isEmpty(channelInfo) && !_isEqual(this.props.channelInfo, channelInfo)

		if (doUpdate) {
			var db = fire.firestore()
			var userRef = db.collection('users').doc(this.context.currentUser.uid)
			return userRef.update({
				"connected_accounts.youtube": channelInfo,
			}).then(() => {
				console.log("Youtube channelInfo successfully updated!")
			}).catch(function(error) {
				// The document probably doesn't exist.
				console.error("Error updating youtube channelInfo: ", error)
			})
		}

	}

	saveChannelInfo(channelInfo) {
		if (!this.isUpdateChannelInfoValid())
			return

		this.isDuplicate(channelInfo.id, (is_duplicate) => {
			if (is_duplicate) {
				alert("The youtube channel is already registered in conation.io")
			} else {
				var db = fire.firestore()
				var userRef = db.collection('users').doc(this.context.currentUser.uid)
				return userRef.update({
					"connected_accounts.youtube": channelInfo,
				}).then(() => {
					console.log("Youtube channelInfo successfully saved!")
				}).catch(function(error) {
					// The document probably doesn't exist.
					console.error("Error saving youtube channelInfo: ", error)
				})
			}
		})
	}

	render() {
		return (


			<AccountRow channelInfo={this.state.channelInfo}
				provider="Youtube"
				checked={this.props.checked}
				onCheckClick={this.props.onCheckClick}
				handleConnectClick={this.handleConnectYoutubeClick} />



		)
	}
}





class AccountRow extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	get_thumbnailURL() {
		if (this.props.channelInfo === undefined)
			return defaultUserIcon
		else if (!this.props.channelInfo.thumbnail)
			return defaultUserIcon
		else
			return this.props.channelInfo.thumbnail
	}

	get_channelName() {
		if (this.props.channelInfo === undefined)
			return `connecting ${this.props.provider}`
		else if (!this.props.channelInfo.name)
			return this.props.provider.toUpperCase()
		else
			return `${this.props.channelInfo.name} (${this.props.provider})`
	}

	onCheckClick = (e) => {
		this.props.onCheckClick(this.props.provider)
	}

	render() {
		return (



<div className="input-group">

	<div className="input-group-prepend">
		<div className="input-group-text pt-0 pb-0 pl-3 pr-3 border-left-0 border-bottom-0">
			{this.props.channelInfo && this.props.channelInfo.id ?
				(<input type="radio" name="main_account"
					value={this.props.provider.toLowerCase()}
					onClick={this.onCheckClick}
					checked={this.props.checked} />)
				: (<input type="radio" name="main_account"
					value={this.props.provider.toLowerCase()}
					onClick={this.onCheckClick}
					checked={this.props.checked} disabled />)
			}
		</div>
	</div>

	<div className="form-control list-group-item" style={{marginLeft: '1px'}}>
		<img className="rounded-circle mr-2" style={{width: 22, height: 22}}
			src={this.get_thumbnailURL()}
			alt="Connected Account" />
		<span>{this.get_channelName()}</span>
		<button className={"btn btn-sm btn-link float-right m-0 p-0" + (this.context.currentUser.emailVerified ? "" : " disabled" )} type="button"
			onClick={this.props.handleConnectClick}>
			{this.props.channelInfo && this.props.channelInfo.id ? "disconnect" : "connect" }</button>
	</div>

</div>



		)
	}
}
/*= End of Youtube Account =*/
/*=============================================<<<<<*/



