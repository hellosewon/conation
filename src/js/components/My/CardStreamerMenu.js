import React, { Component } from 'react'
import PropTypes from 'prop-types'



import fire from '../../../utils/fire'



export default class CardStreamerMenu extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			streamer_checked: '',
			donation_msg: this.props.userInfo.donation_msg,
		}
	}

	componentDidMount() {
		this.setState({ streamer_checked: this.props.userInfo.is_streamer })
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.userInfo.is_streamer !== this.state.streamer_checked) {
			this.setState({ streamer_checked: nextProps.userInfo.is_streamer })
		}
	}

	handleIsStreamerClick = (e) => {
		if (!this.props.userInfo)
			return

		// If is_streamer,
		if (this.props.userInfo.is_streamer === true) {
			if (confirm('Are you sure you want to unregister as streamer?')) {
				// Save (is_streamer = False)
				this.cancelIsStreamer()
				// Will update this.props.userInfo hence automatically re-rendering for unchecked
			}
		} else {
			if (this.state.streamer_checked === false) {
				if (Object.keys(this.props.userInfo.connected_accounts).length > 0) {
					// check and show
					this.setState({ streamer_checked: true })
				} else {
					alert("You need to have one or more connected accounts.")
				}
			} else {
				// uncheck and collapse
				this.setState({ streamer_checked: false })
			}
		}
	}

	cancelIsStreamer() {
		var db = fire.firestore()
		var userRef = db.collection('users').doc(this.context.currentUser.uid)
		return userRef.update({
			"is_streamer": false,
		}).then(() => {
			console.log("is_streamer successfully updated!")
			this.setState({ streamer_checked: false })
				alert("Streamer unregistered.")
		}).catch(function(error) {
			// The document probably doesn't exist.
			console.error("Error canceling is_streamer: ", error)
			// alert("Error updating is_streamer: see log.")
		})
	}

	isSettingIsStreamerValid() {
		if (!this.context.currentUser.emailVerified) {
			alert("Verify your email before connecting external accounts.")
			return false
		}
		if (Object.keys(this.props.userInfo.connected_accounts).length < 1) {
			alert("You need to have one or more connected accounts.")
			return false
		}

		return true
	}

	// isDonationTagDuplicate(donation_tag, callback) {
	// 	var db = fire.firestore()
	// 	var userRef = db.collection("users")

	// 	userRef
	// 	.where("donation_tag", "==", donation_tag)
	// 	.get().then((docs) => {
	// 		callback(docs)
	// 	})
	// 	.catch(function(error) {
	// 		console.log("Error getting url isDuplicate: ", error)
	// 	})
	// }

	updateStreamerInfo(is_streamer, donation_tag, donation_msg) {
		if (!this.isSettingIsStreamerValid())
			return

		var db = fire.firestore()
		var userRef = db.collection('users').doc(this.context.currentUser.uid)
		return userRef.update({
			"is_streamer": is_streamer,
			"donation_tag": donation_tag,
			"donation_msg": donation_msg,
		}).then(() => {
			console.log("is_streamer successfully updated!")
			this.setState({ streamer_checked: is_streamer })
			if (is_streamer)
				alert("Streamer information updated!")
		}).catch(function(error) {
			// The document probably doesn't exist.
			console.error("Error updating is_streamer: ", error)
			// alert("Error updating is_streamer: see log.")
		})
	}

	shouldUpdateStreamerInfo() {
		// If not streamer already, should update!
		if (!this.props.userInfo.is_streamer)
			return true
		// If already streamer && donation_msg changed, should update!
		if (this.state.donation_msg !== this.props.userInfo.donation_msg)
			return true

		return false
	}

	onIsStreamerFormSubmit = (e) => {
		// Should update Streamer Info?
		const donation_msg = e.target.donationMsg.value

		if (!this.shouldUpdateStreamerInfo())
			return

		if (donation_msg.length > 1000) {
			alert("Donation message should be less then 1,000 chracters.")
			return
		}

		this.updateStreamerInfo(true, this.get_donationTag(), donation_msg)
	}

	get_donationTag() {
		// var displayName = this.get_displayName()
		// var tag = displayName.trim().toLowerCase().replace(/ /g,"-")
		// return tag
		return this.props.userInfo.donation_tag
	}

	get_displayName() {
		var main_account = this.props.userInfo.main_account
		var streamerInfo = this.props.userInfo.connected_accounts[main_account]
		if (main_account && streamerInfo)
			return streamerInfo.name
		else
			return "(no main account)"
	}

	get_description() {
		var main_account = this.props.userInfo.main_account
		var streamerInfo = this.props.userInfo.connected_accounts[main_account]
		if (main_account && streamerInfo) {
			var description = streamerInfo.description
			if (description)
				return description
			else
				return "(no description)"
		}
		else
			return "-"
	}

	render_donationTag() {
		var donationTag = this.get_donationTag()
		if (donationTag)
			return <span>#{donationTag}</span>
		else
			return <span>(no donation tag)</span>
	}

	render() {
		return (



<div className="card border rounded">
	<label className="card-header" htmlFor="isStreamerCheck">
		<h5 className="mb-0">
			<div className="form-check">
				<input className="form-check-input" type="checkbox"
					id="isStreamerCheck" onClick={this.handleIsStreamerClick}
					checked={this.state.streamer_checked} />
				<p className="form-check-label">
					{' '}I am a streamer.
				</p>
			</div>
		</h5>
	</label>
	{/* show */}
	<div className={"card-body collapse" + (this.state.streamer_checked ? " show" : "")}>
	<form onSubmit={ this.onIsStreamerFormSubmit } target="dummyframeIsStreamer">
		{/* Donation Tag */}
		<small><strong>DONATION TAG</strong></small><br/>
		{this.render_donationTag()}<br/>
		<div className="spacer10"></div>

		{/* Name */}
		<small><strong>NAME</strong></small><br/>
		<span>{this.get_displayName()}</span><br/>

		{/* Description */}
		<div className="spacer10"></div>
		<small><strong>DESCRIPTION</strong></small><br/>
		<span>{this.get_description()}</span><br/>

		{/* Thank you Message */}
		<div className="spacer10"></div>
		<small><strong>THANK YOU MSG</strong></small><br/>
		<textarea className="form-control" rows="3" name="donationMsg"
			defaultValue={this.props.userInfo.donation_msg}
			placeholder="(no message)"
			onChange={(e) => this.setState({donation_msg: e.target.value})}></textarea><br/>

		<div className="spacer10"></div>
		{this.shouldUpdateStreamerInfo() ?
			(<button type="submit" className="btn btn-primary float-right">SAVE</button>)
			: (<button type="submit" className="btn btn-primary float-right" disabled>SAVE</button>)
		}
		<div className="spacer30"></div>
	</form>
	<iframe width="0" height="0" name="dummyframeIsStreamer" className="d-none"></iframe>
	</div>
	<div className="card-footer bg-white mb-3">
		<small className={this.props.userInfo.is_streamer ? "" : " text-muted" }><strong>STREAMER'S MENU</strong></small>
		{this.props.userInfo.is_streamer ?
		(<button className="btn btn-lg btn-outline-secondary w-100">Overlay Tool for Streamers</button>)
		: (<button className="btn btn-lg btn-outline-secondary w-100 disabled">Overlay Tool for Streamers</button>)}

	</div>
</div>



		)
	}

}