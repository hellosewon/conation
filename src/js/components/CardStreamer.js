import React from "react"
import { NavLink } from 'react-router-dom'



import '../../css/components/CardStreamer.css'



const defaultUserIcon = require('../../img/profile-user.svg');
export default class CardStreamer extends React.Component {

	get_twitchLink() {
		return "https://www.twitch.tv/" + this.props.userInfo.connected_accounts.twitch.id
	}

	get_youtubeLink() {
		return "https://www.youtube.com/channel/" + this.props.userInfo.connected_accounts.youtube.id
	}

	get_thumbnailURL() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].thumbnail
		if (data)
			return data
		else
			return defaultUserIcon
	}

	get_displayName() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].name
		if (data)
			return data
		else
			return "(no name)"
	}

	get_banner() {
		var main_account = this.props.userInfo.main_account
		var data = null
		if (main_account && this.props.userInfo.connected_accounts[main_account])
			data = this.props.userInfo.connected_accounts[main_account].banner
		if (data)
			return "url(" + data + ")"
		// else
		// 	return "#35404f"  // CONATION PRIMARY COLOR
	}

	get_description() {
		var main_account = this.props.userInfo.main_account
		var streamerInfo = this.props.userInfo.connected_accounts[main_account]
		if (main_account && streamerInfo) {
			var description = streamerInfo.description
			if (description)
				return description
			else
				return "(no description)"
		}
		else
			return "-"
	}

	render() {
		return (



<div className="col-6 col-sm-4 col-lg-3 col-xl-2 CardStreamer">
	<div className="card bg-light">
		<div className="banner" style={{background: this.get_banner()}}></div>
			<div className="avatar">
				<img className="border-light" alt=""
				src={this.get_thumbnailURL()} />
			</div>
		<div className="info one-liner">
			<h5 className="title font-raleway">{this.get_displayName()}</h5>
			<div className="desc">{this.get_description()}</div>
		</div>
		<div className="links">
			{this.props.userInfo.connected_accounts.twitch ?
			(
				<a className="btnAccount bg-twitch-primary text-light"
					target="_blank" href={this.get_twitchLink()}>
					<i className="fab fa-twitch"></i></a>
			) : ""}
			{this.props.userInfo.connected_accounts.youtube ?
			(
				<a className="btnAccount bg-youtube-primary text-light"
					target="_blank" href={this.get_youtubeLink()}>
					<i className="fab fa-youtube"></i></a>
			) : ""}

		</div>
		<hr className="mb-0" />
		<div className="bottom">
			<NavLink className="btn btn-outline-secondary w-100"
				exact to={"/donate#"+this.props.userInfo.donation_tag}>DONATE</NavLink>
		</div>
	</div>
	<div className="spacer10"></div>
</div>



		)
	}
}




