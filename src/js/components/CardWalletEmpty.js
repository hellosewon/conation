import React from "react"
import { NavLink } from 'react-router-dom'


// import '../../css/components/CardWallet.css'


export default class CardWallet extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
		}
	}

	render() {
		return (


<div>
	{/* Wallet */}
	<div className="card text-white bg-cona-primary mb-3 rounded" style={{minHeight: '220px'}}>
		<div className="card-header bg-transparent">No wallet</div>
		<div className="card-body text-center">
			<br/>
			{this.props.isLoggedIn ?
				<button className="btn btn-lg btn-outline-light">Create CONA wallet</button>
				: (
					<div>
					<NavLink className="btn btn-outline-light btn-lg"
						exact to="/signin">Sign in</NavLink>
					<span className="ml-3 mr-3">OR</span>
					<NavLink className="btn btn-outline-light btn-lg"
						exact to="/signup">Sign up</NavLink>
					</div>
				) }
		</div>
	</div>
</div>

		)
	}
}