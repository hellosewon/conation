import React, { Component } from 'react'



import CardFeaturedStreamer from './CardFeaturedStreamer'
import CardStreamer from './CardStreamer'



import {_nwc} from '../../utils/myUtil'
export default class SegmentStreamers extends Component {

	render_CardFeaturedStreamers(data) {
		if (data) {
			var arr = []
			Object.keys(data).forEach(function(key) {
				arr.push(data[key]);
			});
			var x = 0
			var result = <div className="row little-gutters"> {
				arr.map((i) => {
					x++
					// return <CardFeaturedStreamer key={x} userInfo={i} />
					return <CardFeaturedStreamer key={x} userInfo={i} />
				})
			}</div>
			return result
		} else {
			return "(No registered streamers)"
		}
	}

	render_CardStreamers(data) {
		var arr = []
		Object.keys(data).forEach(function(key) {
			arr.push(data[key]);
		});
		var x = 0
		var searchText = ""
		if (this.props.search)
		 	searchText = this.props.search.trim().toLowerCase()
		var result = <div className="row little-gutters"> {
			arr.map((i) => {
				x++
				var eYoutube = i.connected_accounts["youtube"]
				var eTwitch = i.connected_accounts["twitch"]
				var doShow = (eYoutube && i.connected_accounts["youtube"]
						.name.toLowerCase().includes(searchText)) ||
					(eTwitch && i.connected_accounts["twitch"]
						.name.toLowerCase().includes(searchText))
				if (doShow)
					return <CardStreamer key={x} userInfo={i} />
				return ""
			})
		}</div>
		return result
	}

	render() {
		return (

<div>
	<div className="row">
		<div className="col-12 d-flex justify-content-between">
			<h3>{this.props.segTitle}{this.props.showCnt && " (" + _nwc(this.props.count) + ")" }</h3>
			{this.props.showBtn &&
				<a className="btn btn-outline-secondary"
					href="/streamers">See all</a>
			}

		</div>
	</div>
	<hr/>
	{ this.props.isFeatured ?
		this.render_CardFeaturedStreamers(this.props.data)
		: this.render_CardStreamers(this.props.data)}
	<div className="spacer45"></div>
</div>


		)
	}
}