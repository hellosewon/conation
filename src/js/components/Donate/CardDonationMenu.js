import React, { Component } from 'react'
import PropTypes from 'prop-types'



// import fire from '../../../utils/fire'
import '../../../css/components/Donate/CardDonationMenu.css'


var _isEqual = require('lodash/isEqual')

export default class CardDonationMenu extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			accepting_currs: [],
			donation_currency: null,
			qrcodes: {},
		}
	}

	componentDidMount() {
		this.preRender_acceptingCurrencies()
	}

	shouldComponentUpdate(nextProps, nextState) {
		return !_isEqual(this.state, nextState) ||
				!_isEqual(this.props.streamerInfo, nextProps.streamerInfo)
	}

	handleCurrencyClick = (unit) => {
		// if (!this.context.currentUser) {
		// 	alert("You need to sign in to make donations.")
		// 	return
		// }

		if (this.state.donation_currency === unit)
			this.setState({ donation_currency: null })
		else
			this.setState({ donation_currency: unit })
	}

	getwWalletInfo(unit, address) {
		if (!['rETH'].includes(unit))
			return false

	}

	getDonationCurrencyData(unit) {
		let data = null
		switch(unit) {
			case 'rETH':
				data = {
					color: "bg-eth-primary",
					icon: "fa-ethereum",
					unit: "rETH",
				}
				break
			case 'ETH':
				data = {
					color: "bg-eth-primary",
					icon: "fa-ethereum",
					unit: "ETH",
				}
				break
			case 'BTC':
				data = {
					color: "bg-btc-primary",
					icon: "fa-btc",
					unit: "BTC",
				}
				break
			default:
				data = null
		}
		return data
	}

	generateQRCode(addr, callback) {
		if (!addr || addr === "") {
			callback(null)
			return
		}
		var QRCode = require('qrcode')

		var opts = {
			errorCorrectionLevel: 'H',
			type: 'image/png',
			margin: 4,
			scale: 5,
		}
		// console.log(QRCode)
		QRCode.toDataURL(addr, opts, (err, url) => {
			if (err) throw err
			callback(url)
		})
	}

	preRender_acceptingCurrencies() {
		var wallets_accepting = this.props.streamerInfo.wallets_accepting

		var qrcodes = {}
		var accepting_currs = []
		for (var i=0; i < wallets_accepting.length; i++) {
			let unit = wallets_accepting[i]
			let data = this.getDonationCurrencyData(unit)
			if (data) {
				accepting_currs.push(<DonationCurrency key={i} data={data}
									selected={this.state.donation_currency === data.unit}
									onClick={this.handleCurrencyClick} />)
				this.generateQRCode(this.props.streamerInfo.wallets[unit], (url) => {
					qrcodes[unit] = url
					if (Object.keys(qrcodes).length === wallets_accepting.length) {
						this.setState({ qrcodes: qrcodes })
						// console.log(qrcodes)
					}
				})
			}
		}
		this.setState({ accepting_currs: accepting_currs })
	}

	render_acceptingCurrencies() {
		let renderList = []
		for (var i=0; i < this.state.accepting_currs.length; i++) {
			renderList.push(
				<li key={i} className={"liCurrency" + (this.state.accepting_currs[i].props.data.unit === this.state.donation_currency ? " selected" : "")}>
					{this.state.accepting_currs[i]}
				</li>
			)
		}
		return (
			<ul className="currencies m-0 p-0">
				{renderList}
			</ul>
		)
	}

	render_donationInput() {
		const alertNoneComponent = <div className="alert alert-info mb-1">This streamer is currently not accepting donations.</div>
		var wallets_accepting = this.props.streamerInfo.wallets_accepting

		if (!wallets_accepting)
			return alertNoneComponent

		if (wallets_accepting.length === 0)
			return alertNoneComponent

		return (
			<div className="card border rounded CardDonationMenu">
				<div className="card-body text-center bg-light mb-0 pb-2">
					{this.render_acceptingCurrencies()}

				</div>
				<div className={"bg-light collapse" + (this.state.donation_currency ? " show" : "")}>
					<DonationInput unit={this.state.donation_currency}
						qrcode={this.state.qrcodes[this.state.donation_currency]} />
				</div>
			</div>
		)
	}

	render() {
		return (

<div>
	<small><strong>DONATE WITH:</strong></small>
	{this.render_donationInput()}
</div>

		)
	}

}


class DonationCurrency extends Component {

	onCurrencyClick = (e) => {
		this.props.onClick(this.props.data.unit)
	}

	render() {
		return (

<span onClick={this.onCurrencyClick}>
	<span className={"btnCurrency text-light text-center " + this.props.data.color}>
		<i className={"fab fa-lg " + this.props.data.icon}></i>
	</span><br/>
	<span className="unit"><small>{this.props.data.unit}</small></span>
</span>

		)
	}
}

class DonationInput extends Component {

	onDonateClick = (e) => {
		alert(12)
	}

	render_qrcode() {
		if (!this.props.qrcode)
			return "(no address)"
		return (
			<div className="text-center">
				<img className="img-thumbnail" alt="QR Code" style={{width: '50%'}} 
					src={this.props.qrcode} />
			</div>
		)
	}

	render() {
		return (
<div>
	<hr className="m-0 p-0" />
	<div className="bg-light card-body text-center">
		<small><strong>QR CODE ({this.props.unit})</strong></small><br/>
		{/* QR Code */}
		{this.render_qrcode()}

		
	</div>
</div>
		)
	}
}
/*
		<br/>
		<button className="btn btn-lg btn-secondary w-100"
			onClick={this.onDonateClick}>DONATE</button>
*/
