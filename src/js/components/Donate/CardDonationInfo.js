import React, { Component } from 'react'
import PropTypes from 'prop-types'



// import fire from '../../../utils/fire'
import '../../../css/components/Donate/CardDonationInfo.css'



const defaultUserIcon = require('../../../img/profile-user.svg')
export default class CardDonationInfo extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	get_twitchLink() {
		return "https://www.twitch.tv/" + this.props.streamerInfo.connected_accounts.twitch.id
	}

	get_youtubeLink() {
		return "https://www.youtube.com/channel/" + this.props.streamerInfo.connected_accounts.youtube.id
	}

	get_thumbnailURL() {
		var main_account = this.props.streamerInfo.main_account
		var data = null
		if (main_account && this.props.streamerInfo.connected_accounts[main_account])
			data = this.props.streamerInfo.connected_accounts[main_account].thumbnail
		if (data)
			return data
		else
			return defaultUserIcon
	}

	get_displayName() {
		var main_account = this.props.streamerInfo.main_account
		var data = null
		if (main_account && this.props.streamerInfo.connected_accounts[main_account])
			data = this.props.streamerInfo.connected_accounts[main_account].name
		if (data)
			return data
		else
			return "(no name)"
	}

	get_banner() {
		var main_account = this.props.streamerInfo.main_account
		var data = null
		if (main_account && this.props.streamerInfo.connected_accounts[main_account])
			data = this.props.streamerInfo.connected_accounts[main_account].banner
		if (data)
			return "url(" + data + ")"
		// else
		// 	return "#35404f"  // CONATION PRIMARY COLOR
	}

	get_description() {
		var main_account = this.props.streamerInfo.main_account
		var streamerInfo = this.props.streamerInfo.connected_accounts[main_account]
		if (main_account && streamerInfo) {
			var description = streamerInfo.description
			if (description)
				return description
			else
				return "(no description)"
		}
		else
			return "-"
	}

	render_donationMsg() {
		const dm = this.props.streamerInfo.donation_msg
		if (!dm || dm === "")
			return <span className="text-muted">(no message)</span>
		else
			return <span>{dm}</span>
	}

	render() {
		return (



<div className="CardDonationInfo">
	<div className="card border rounded bg-light">

		<div className="card-header-x">
			<div className="banner border-bottom" style={{background: this.get_banner()}}></div>
			<div className="avatar">
				<img className="border-white bg-light" alt="" src={this.get_thumbnailURL()} />
			</div>

			<div className="info one-liner">
				<h5 className="title font-raleway">{this.get_displayName()}</h5>
				<div className="desc">{this.get_description()}</div>
			</div>

			<div className="links">
				{this.props.streamerInfo.connected_accounts.twitch ?
				(
					<a className="btnAccount bg-twitch-primary text-light"
						target="_blank" href={this.get_twitchLink()}>
						<i className="fab fa-twitch"></i></a>
				) : ""}
				{this.props.streamerInfo.connected_accounts.youtube ?
				(
					<a className="btnAccount bg-youtube-primary text-light"
						target="_blank" href={this.get_youtubeLink()}>
						<i className="fab fa-youtube"></i></a>
				) : ""}

			</div>
		</div>
		<hr />
		<div className="card-body pt-1">
			{/* Donation Message */}
			<center><small><strong>THANK YOU MESSAGE</strong></small></center><br/>
			{this.render_donationMsg()}
		</div>
	</div>
</div>



		)
	}
}




