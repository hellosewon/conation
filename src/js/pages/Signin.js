import React, { Component } from 'react'
import { Redirect } from 'react-router'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'



import fire from '../../utils/fire'
import Footer from '../components/Footer'
import Header from '../components/Header'
import Loading from '../components/Loading'
// import '../../css/pages/Signin.css'



import {_validEmail, _validPassword} from '../../utils/myUtil'
export default class Signin extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			// data: DD.data_for_streamers // TODO: Change to real DB
			isLoading: false,
			forgottenEmail: "",
		}
	}

	onFormSubmit = (event) => {
		// TODO: Front Check
		// Length 0 < x, 6 < x
		// Valid Email
		const email = event.target.theEmail.value
		const password = event.target.thePassword.value

		if (!_validEmail(email))
			return

		if (!_validPassword(password))
			return

		this.setState({ isLoading: true })
		fire.auth().signInWithEmailAndPassword(email, password)
		.then((user) => {
			this.setState({ isLoading: false })
			// var user = fire.auth().currentUser
			// alert("Welcome: " + JSON.stringify(user))
			// logUser(user) // Optional
		}, (error) => {
			this.setState({ isLoading: false })
			// Handle Errors here.
			var errorCode = error.code
			var errorMessage = error.message
			switch(errorCode) {
				case 'auth/invalid-email':
					alert('Invalid email.')
					// TODO: Highlight Email + Display Message
					break
				case 'auth/user-disabled':
					alert('User disabled.')
					break
				case 'auth/user-not-found':
					alert('User not found.')
					// TODO: Go to or expand to or Suggest Register
					break
				case 'auth/wrong-password':
					alert('Wrong password.')
					// TODO: Highlight Password Input/Forgot password? + Display Message
					break
				default:
					alert('Authentication failed: ' + errorCode)
					console.log(errorMessage)
			}
		})
		return false
	}

	updateForgottenEmail = (e) => {
		this.setState({ forgottenEmail: e.target.value })
	}

	onResetPassword = (e) => {
		var actionCodeSettings = {
			// url: 'https://www.example.com/?email=user@example.com',
		}
		var email = e.target.theEmail.value
		fire.auth().sendPasswordResetEmail(
			email, actionCodeSettings)
		.then(function() {
			alert("Password reset email sent.")
		})
		.catch(function(error) {
			// Error occurred. Inspect error.code.
			alert(error)
		})
	}


	render() {
		// Redirect to my page if logged in
		if (this.context.currentUser != null)
			return <Redirect push to="my" />
		else return (


<div className="Signin">
	<Header />
	{this.state.isLoading && <Loading />}
	<div className="spacer45"></div>


	<div className="container-fluid" style={{maxWidth:'480px'}}>
		<form onSubmit={ this.onFormSubmit } target="dummyframeSignIn">
			<div className="row">
				<div className="col-12">
					<h3>SIGN IN</h3>
					<hr/>
					<div className="form-group">
						<div className="parent-input-with-icon">
							<i className="far fa-user icon-before-input"></i>
							<input type="email" className="form-control input-after-icon"
								placeholder="Enter email" name="theEmail"
								onChange={e => this.updateForgottenEmail(e)}/>
						</div>
						<div className="parent-input-with-icon t--1">
							<i className="fas fa-lock icon-before-input"></i>
							<input type="password" className="form-control input-after-icon"
								placeholder="Enter password" name="thePassword"/>
						</div>
					</div>
				</div>
			</div>
			<div className="row">
				<div className="col-12">
					<button type="submit" className="btn btn-lg btn-outline-secondary w-100">Let's go.</button>
				</div>
			</div>
			<div className="spacer30"></div>
			<div className="text-center">
				<a href="#" className="btn btn-link btn-sm text-muted" id="forgotPassword"
					data-toggle="modal" data-target="#forgotPasswordModal">Forgot password?</a>
			</div>
		</form>
		<iframe width="0" height="0" name="dummyframeSignIn" className="d-none"></iframe>
		<div className="spacer45"></div>

		{/* Need an Account?*/}
		<div className="row">
			<div className="col-12">
				<h3>NEED ACCOUNT?</h3>
				<hr/>
				<NavLink className="btn btn-lg btn-outline-secondary w-100"
					exact to="/signup">Sign up (takes 5 seconds)</NavLink>
			</div>
		</div>
	</div>
	<div className="spacer45"></div>


	{/* FORGOT PASSWORD MODAL */}
	<div className="modal fade" tabIndex="-1" role="dialog" id="forgotPasswordModal">
		<div className="modal-dialog" role="document">
			<div className="modal-content">
				<form onSubmit={ this.onResetPassword } target="dummyframeForgotEmail">
				<div className="modal-header">
					<h5 className="modal-title">Forgot Password?</h5>
					<button type="button" className="close" data-dismiss="modal" aria-label="Close">
						<span>&times;</span>
					</button>
				</div>
				
				<div className="modal-body">
					{/* Warning Statement */}
					<div className="alert alert-info" role="alert">
						A password reset email will be sent to the below address.
					</div>

					{/* Text Address */}
					<div className="form-group">
						<fieldset>
							<small><strong>EMAIL</strong></small>
							<input className="form-control" name="theEmail"
								type="text" placeholder="Enter email"
								value={this.state.forgottenEmail}
								onChange={e => this.updateForgottenEmail(e)} />
						</fieldset>
					</div>
				</div>
				<div className="modal-footer">
					<button type="button" className="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" className="btn btn-secondary">Reset password</button>
				</div>
				</form>
				<iframe width="0" height="0" name="dummyframeForgotEmail" className="d-none"></iframe>
			</div>
		</div>
	</div>
	<Footer />
</div>


		)
	}
}
