import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import { NavLink } from 'react-router-dom'



import fire from '../../utils/fire'
import Footer from '../components/Footer'
import Header from '../components/Header'
// import Loading from '../components/Loading'
import CardWallet from '../components/CardWallet'
import CardWalletEmpty from '../components/CardWalletEmpty'
import TransactionsTable from '../components/TransactionsTable'
// import '../../css/pages/Signin.css'



// var DD = require('../dummydata')
export default class Wallets extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			// data: DD.data_for_streamers // TODO: Change to real DB
			userInfo: undefined,
			// isLoading: false,
			// forgottenEmail: "",
		}
	}

	getUserInfo() {
		var db = fire.firestore()
		var userRef = db.collection('users').doc(this.context.currentUser.uid)
		userRef.get().then((doc) => {
			if (doc.exists) {
				//alert(JSON.stringify(doc.data()))
				this.setState({userInfo: doc.data()})
			} else {
				this.setState({userInfo: null})
			}
		}).catch((error) => {
			alert("Error geting document: see log.")
			console.log("Error getting document: ", error)
		})
	}

	render_eth_wallet() {
		if (this.state.userInfo === undefined && this.context.currentUser){
			// Loading
			return <span>(Loading User Infomation)</span>
		}
		else if (this.state.userInfo === undefined && this.context.currentUser === null)
			// 1:1 Cannot find doc (Not logged in)
			return <CardWalletEmpty isLoggedIn={false} />
		else if (this.state.userInfo && (!this.state.userInfo.wallets["rETH"] || this.state.userInfo.wallets["rETH"] === ""))
			// Empty Wallet (Create Button)
			return <CardWalletEmpty isLoggedIn={true} />
		else if (this.state.userInfo && this.state.userInfo.wallets["rETH"] && this.state.userInfo.wallets["rETH"] !== "")
			// Wallet Found
			return <CardWallet address={this.state.userInfo.wallets["rETH"]} name="My rETH Wallet" unit="rETH" />
	}

	render_transaction_table() {
		if (this.state.userInfo === undefined && this.context.currentUser)
			// Loading
			return <span>(Loading Transaction History)</span>
		else if (this.state.userInfo === undefined && this.context.currentUser === null)
			// 1:1 Cannot find doc (Not logged in)
			return <span>(Not logged in)</span>
		else if (this.state.userInfo && (!this.state.userInfo.wallets["rETH"] || this.state.userInfo.wallets["rETH"] === ""))
			// Empty Wallet (Create Button)
			return <span>(No wallet)</span>
		else if (this.state.userInfo && this.state.userInfo.wallets["rETH"] && this.state.userInfo.wallets["rETH"] !== "")
			return <TransactionsTable currencyFilter={["rETH", "cona"]} ioFilter={0} limitFilter={5} />
	}

	render() {
		return (



<div className="Wallet">

	{/* Bound Functions */}
	{this.context.currentUser && !this.state.userInfo && this.getUserInfo()}
	<Header />
	{/* this.state.isLoading && <Loading /> */}
	<div className="spacer45"></div>


	<div className="container-fluid" style={{maxWidth:'480px'}}>
			<div className="row">
				<div className="col-12">
					<h3>MY WALLETS</h3>
					<hr/>
					<div className="alert alert-secondary mb-1">
						<strong>Step 3.</strong> Create an ethereum wallet. <u>Learn more.</u>
					</div>
					<div className="alert alert-secondary">
						<strong>Step 4.</strong> Create a cona wallet. <u>Learn more.</u>
					</div>
					{this.render_eth_wallet()}
				</div>
			</div>

		<div className="spacer45"></div>

	</div>

	<div className="container-fluid" style={{maxWidth:'960px'}}>
			<div className="row">
				<div className="col-12">
					<h3>TRANSACTIONS</h3>
					<hr/>
					{this.render_transaction_table()}

					<button type="button" className="btn btn-lg btn-outline-secondary float-right"
						onClick="">See more</button>
				</div>
			</div>

		<div className="spacer45"></div>

	</div>
	<div className="spacer45"></div>

	<Footer />
</div>


		)
	}
}
