import React, { Component } from 'react'



import fire from '../../utils/fire'



const defaultDonaPic = require('../../img/dona_pic.png')
export default class OverlayTemp extends Component {

	constructor(props) {
		super(props)

		this.state = {
			alert_msg: null,
		}
	}

	componentDidMount() {
		this.getAlerts()
	}

	getAlerts() {
		var db = fire.firestore()
		var alertsRef = db.collection('users').doc("is6o68GcjobEl91GF6y5i0mQD6W2")
						.collection('overlay_alerts')


		alertsRef
		.where("exposed", "==", false)
		.orderBy("timestamp")
		.limit(1)
		.onSnapshot((docs) => {
			if (docs.empty) {
				this.setState({alert_msg: null})
				console.log("No overlay alerts")
			} else {
				this.setState({alert_msg: docs.docs[0].data().msg})
				console.log(docs)
				setTimeout(() => {this.updateAlertExposed(docs.docs[0].id)}, 3000);
			}		
		})
	}

	updateAlertExposed(doc_id) {
		var db = fire.firestore()
		var alertRef = db.collection('users').doc("is6o68GcjobEl91GF6y5i0mQD6W2")
						.collection('overlay_alerts').doc(doc_id)

		alertRef.update({
			"exposed": true,
		}).then(() => {
			console.log("exposed successfully updated!")
			// this.setState({ streamer_checked: false })
			// 	alert("Streamer unregistered.")
		}).catch(function(error) {
			// The document probably doesn't exist.
			console.error("Error updating exposed: ", error)
			// alert("Error updating is_streamer: see log.")
		})
	}

	addTestAlert = () => {
		var db = fire.firestore()
		var alertRef = db.collection('users').doc("is6o68GcjobEl91GF6y5i0mQD6W2")
						.collection('overlay_alerts')

		const firebase = require("firebase")
		// Required for side-effects
		require("firebase/firestore")

		alertRef.add({
			alert_type: 0,
			msg: "This is a test donation msg~",
			exposed: false,
			timestamp: firebase.firestore.FieldValue.serverTimestamp(),
		})
	}

	render_alert() {
		if (!this.state.alert_msg)
			return
		return (
			<div className="text-center">
				<img src={defaultDonaPic} width="300" alt="" /><br/>
				<span>{this.state.alert_msg}</span>
			</div>
		)
	}


	render() {
		return (


<div className="OverlayTemp" style={{width: '1280px', height: '720px',
								minWidth: '1280px', minHeight: '720px',
								border: '1px solid'}}>
	<h1>CONATION.IO TEST yo (1280x720) <button className="btn" onClick={this.addTestAlert}>Test Dona</button></h1>
	<br/><br/>
	{this.render_alert()}
	
</div>


		)
	}
}
