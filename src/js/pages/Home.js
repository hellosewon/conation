import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'



import fire from '../../utils/fire'
import Footer from '../components/Footer'
import Header from '../components/Header'
// import SearchBar from '../components/SearchBar'
import SegmentStreamers from '../components/SegmentStreamers'
import CardComarketItem from '../components/CardComarketItem'
// import '../../css/pages/Home.css'



import anime from 'animejs'
import {_nwc} from '../../utils/myUtil'
var DD = require('../dummydata')
export default class Home extends Component {

	constructor(props) {
		super(props)

		this.state = {
			data: DD.data_for_home, // TODO: Change to real DB
			all_streamers: [],
		}
	}

	componentDidMount() {
		this.getStreamers(12)
		this.animateJumbo()
		// this.animateCrazy()

	}

	animateJumbo() {
		anime.timeline().add({
			targets: '.Home .amtn1 .letter',
			opacity: [0,1],
			easing: "easeInOutQuad",
			duration: 400,
			delay: function(el, i) {
				return 50 * (i+1)
			}
		}).add({
			targets: '.Home .amtn2 .letter',
			opacity: [0,1],
			easing: "easeInOutQuad",
			duration: 400,
			delay: function(el, i) {
				return 50 * (i+1)
			}
		}).add({
			targets: '.Home .amtn3',
			opacity: [0,1],
			duration: 1000,
			easing: "easeInOutQuad",
			delay: function(el, i) {
				return 1000 * (i)
			}
		})
	}

	animateCrazy() {
		anime({
			targets: 'div',
			translateX: [
				{ value: 100, duration: 1200 },
				{ value: 0, duration: 800 }
			],
			rotate: '1turn',
			backgroundColor: '#FFF',
			duration: 2000,
			loop: true
		})
	}

	spanify(str) {
		var arry = []
		for (var i=0; i<str.length; i++) {
			arry.push(<span key={i} className='letter'>{str[i]}</span>)
		}
		return arry
	}

	getStreamers(limit) {
		var db = fire.firestore()
		var streamersRef = db.collection("users")

		streamersRef
		.where("is_streamer", "==", true)
		.where("main_account", ">=", "")
		.limit(limit)
		.get().then((docs) => {
			// console.log("Streamers=" + docs.size)
			// console.log(docs)
			let arryStreamers = []
			docs.forEach(function(doc) {
				arryStreamers.push(doc.data())
        	})
        	if (this.refs.Home) 
        		this.setState({ all_streamers: arryStreamers })
        	// console.log(this.state.all_streamers)
		})
		.catch(function(error) {
			console.log("Error getting Streamers: ", error)
		})
	}

	render_CardComarketItems(data) {
		var arr = []
		Object.keys(data).forEach(function(key) {
			arr.push(data[key])
		})
		var result = <div className="row little-gutters"> {
			arr.map(i => <CardComarketItem key={i._id} title={i.title}
				desc={i.desc} count_like={i.count_like} price={i.price} />)
		}</div>
		return result
	}

	render() {
		return (


<div className="Home" ref="Home">
	<Header />
	{/* <SearchBar placeholder="Search for streamers" /> */}

	<div className="jumbotron mb-0">
		<h1 className="display-4 amtn1">{this.spanify("The way to support,")}</h1>
		<h1 className="display-4 amtn2">{this.spanify("A reason to stream.")}</h1>
		<p className="lead">
			<span className="amtn3">
				<i className="far fa-user"></i>
				<span> {_nwc(this.state.data['count_total_users'])}</span>
					{' '}users strong,{' '}
			</span>
			<br className="d-block d-sm-none"/>
			<span className="amtn3">
				<i className="fab fa-gg"></i>
				<span> {_nwc(this.state.data['count_total_conas'])}</span>
					{' '}sent. (≈ ₩<span> {_nwc(Math.round(this.state.data['cona_exrate'] * this.state.data['count_total_conas']))}</span>)
			</span>
		</p>
		<hr className="my-4" />
		<div className="amtn3">
			<p><i className="fab fa-gg"></i> is a currency used to support your favourite streamers.</p>
			<p className="lead">
				<NavLink className="btn btn-outline-secondary btn-lg amtn3"
					exact to="/whitepaper">Learn more</NavLink></p>
		</div>
	</div>



	<div className="spacer45"></div>




	<div className="container-fluid" style={{maxWidth:'1400px'}}>
		{/*
		<StreamersSegment segTitle="FEATURED STREAMERS"
			isFeatured="1"
			allCount={this.getFeaturedStreamers(6).length}
			data={this.getFeaturedStreamers(6)} />
		*/}

		<SegmentStreamers segTitle="STREAMERS"
			count={this.state.all_streamers.length}
			showCnt={false} showBtn={true}
			data={this.state.all_streamers} />
	</div>
	<div className="spacer45"></div>

	<div className="container-fluid" style={{maxWidth:'1400px'}}>


		<div className="row">
			<div className="col-12 d-flex justify-content-between">
				<h3>COMARKET</h3>
				<a className="btn btn-outline-secondary"
					href="/whitepaper">See all ({this.state.data['count_all_comarket_items']})</a>
			</div>
		</div>
		<hr/>
		{ this.render_CardComarketItems(this.state.data['comarket_items']) }


		<div className="spacer45"></div>


	</div>
	<Footer />
</div>


		)
	}
}

