import React, { Component } from 'react'



import fire from '../../utils/fire'
import Footer from '../components/Footer'
import Header from '../components/Header'
import SearchBar from '../components/SearchBar'
import SegmentStreamers from '../components/SegmentStreamers'
import '../../css/pages/Streamers.css'



var DD = require('../dummydata')
export default class Streamers extends Component {
	constructor(props) {
		super(props)

		this.state = {
			data: DD.data_for_streamers, // TODO: Change to real DB
			all_streamers: [],
			search: "",
		}
	}

	componentDidMount() {
		// Warning: 120 limit for all streamers
		this.getStreamers(120)
	}

	getFeaturedStreamers(size) {
		var allStreamers = this.state.all_streamers
		if (size > allStreamers.length)
			size = allStreamers.length
		return allStreamers.slice(0, size)
	}

	getStreamers(limit) {
		var db = fire.firestore()
		var streamersRef = db.collection("users")

		streamersRef
		.where("is_streamer", "==", true)
		.where("main_account", ">=", "")
		.limit(limit)
		.get().then((docs) => {
			// console.log("Streamers=" + docs.size)
			// console.log(docs)
			let arryStreamers = []
			docs.forEach(function(doc) {
				arryStreamers.push(doc.data())
        	});
        	if (this.refs.Streamers) 
        		this.setState({ all_streamers: arryStreamers })
        	// console.log(this.state.all_streamers)
		})
		.catch(function(error) {
			console.log("Error getting Streamers: ", error)
		})
	}

	onSearchChange = (search) => {
		this.setState({search: search})
	}

	render() {
		return (


<div className="Streamers" ref="Streamers">
	<Header />
	<SearchBar placeholder="Search for streamers"
		onChange={this.onSearchChange} />
	<div className="spacer45"></div>


	<div className="container-fluid" style={{maxWidth:'1400px'}}>
		{/*
		<SegmentStreamers segTitle="FEATURED STREAMERS"
			isFeatured="1"
			allCount={this.getFeaturedStreamers(6).length}
			data={this.getFeaturedStreamers(6)} />
		*/}

		{/*
		<SegmentStreamers segTitle="YOU FOLLOW"
			allCount={this.state.data['count_following_streamers']}
			data={this.state.data['following_streamers']} />
		*/}

		<SegmentStreamers segTitle="STREAMERS"
			count={this.state.all_streamers.length}
			showCnt={true} showBtn={false}
			data={this.state.all_streamers}
			search={this.state.search} />
	</div>
	<div className="spacer45"></div>
	
	<Footer />
</div>


		)
	}
}

