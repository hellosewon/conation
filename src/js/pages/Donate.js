import React, { Component } from 'react'
// import { Redirect } from 'react-router'
// import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'



import fire from '../../utils/fire'
import Footer from '../components/Footer'
import Header from '../components/Header'
// import Loading from '../components/Loading'
// import CardStreamer from '../components/CardStreamer'
import CardDonationInfo from '../components/Donate/CardDonationInfo'
import CardDonationMenu from '../components/Donate/CardDonationMenu'
// import '../../css/pages/Signin.css'



export default class Donate extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			isLoading: false,
			donationTag: Object.keys(this.parseHash())[0],
			// userInfo: undefined,
			streamerInfo: undefined,
		}
	}

	parseHash() {
		const data = location.hash
		const qs = require('query-string')
		const parsedHash = qs.parse(data)
		return parsedHash
	}

	componentDidMount() {
		// console.log(this.state.donationTag)
		if (!this.state.streamerInfo)
			this.getStreamerInfo()
	}

	// onFormSubmit = (event) => {

	// }

	// getUserInfo() {
	// 	var db = fire.firestore()
	// 	var userRef = db.collection('users').doc(this.context.currentUser.uid)
	// 	userRef.onSnapshot((doc) => {
	// 		if (doc.exists) {
	// 			//alert(JSON.stringify(doc.data()))
	// 			if (this.refs.Donate) 
	// 				this.setState({userInfo: doc.data()})
	// 		} else {
	// 			if (this.refs.Donate) 
	// 				this.setState({userInfo: null})
	// 		}
	// 	})
	// }

	getStreamerInfo() {
		var db = fire.firestore()
		var streamersRef = db.collection("users")

		streamersRef
		.where("donation_tag", "==", this.state.donationTag)
		.limit(1)
		.get().then((docs) => {
			// console.log(docs)
        	if (this.refs.Donate && docs.size === 1) 
        		this.setState({ streamerInfo: docs.docs[0].data() })
		})
		.catch(function(error) {
			console.log("Error getting streamerInfo: ", error)
		})
	}

	onDonateClick = (e) => {
		if (!this.context.currentUser) {
			alert("You need to sign in before donating.")
			return
		}
		alert("Donation process in development.")
	}

	render_alert1() {
		return (
			<div className="alert alert-info mb-1">
				<strong>This is</strong> donation alert. <u>Learn why.</u>
			</div>
		)
	}

	render_donationInfo() {
		if (!this.state.streamerInfo)
			return "(Loading streamer info...)"
		return (<CardDonationInfo streamerInfo={this.state.streamerInfo} />)
	}

	render_donationMenu() {
		if (!this.state.streamerInfo)
			return "(Loading donation menu...)"
		return (<CardDonationMenu streamerInfo={this.state.streamerInfo} />)
	}

	render() {
		// Redirect to my page if logged in
		return (


<div className="Donate" ref="Donate">
	<Header />
	{/* Bound Functions */}
	{/*!this.state.streamerInfo && this.getStreamerInfo()*/}
	{/*this.context.currentUser && !this.state.userInfo && this.getUserInfo()*/}
	<div className="spacer45"></div>


	<div className="container-fluid" style={{maxWidth:'480px'}}>
		<div className="row">
			<div className="col-12">
				<div className="spacer30"></div>
				{this.render_donationInfo()}
				<div className="spacer30"></div>
				{this.render_donationMenu()}
			</div>
		</div>

		<div className="spacer45"></div>
	</div>
	<div className="spacer15"></div>
	<Footer />
</div>


		)
	}
}
