import React, { Component } from 'react'
import { Redirect } from 'react-router'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'



import fire from '../../utils/fire'
import Footer from '../components/Footer'
import Header from '../components/Header'
import Loading from '../components/Loading'
// import '../../css/pages/Signup.css'



import {_validEmail, _validPassword} from '../../utils/myUtil'
var pwdmeter = require('../../utils/pwdmeter')
export default class Signup extends Component {

	static contextTypes = {
		currentUser: PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			isLoading: false,
			pwScore: {score: 0, complexity: "Too short"},
		}
	}

	onFormSubmit = (event) => {
		// Warning: Only frontend check for password
		const email = event.target.theEmail.value
		const password = event.target.thePassword.value

		if (!_validEmail(email))
			return

		if (!_validPassword(password))
			return

		if (this.state.pwScore.score < 40) {
			alert("Weak password.")
			return
		}

		// console.log('Email:', email)
		// console.log('Password:', password)
		this.setState({ isLoading: true })
		fire.auth().createUserWithEmailAndPassword(email, password)
		.then((user) => {
			this.setState({ isLoading: false })
			// var user = fire.auth().currentUser
			// alert("Welcome: " + JSON.stringify(user))
			// logUser(user) // Optional
		}, (error) => {
			this.setState({ isLoading: false })
			// Handle Errors here.
			var errorCode = error.code

			var errorMessage = error.message
			switch(errorCode) {
				case 'auth/email-already-in-use':
					alert('Email already in use.')
					// TODO: Highlight Email + Display Message
					break
				case 'auth/invalid-email':
					alert('Invalid email.')
					// TODO: Highlight Email + Display Message
					break
				case 'auth/operation-not-allowed':
					// Thrown if email/password accounts are not enabled.
					// Enable email/password accounts in the Firebase Console,
					// under the Auth tab.
					alert('Operation not allowed.')
					break
				case 'auth/weak-password':
					alert('Weak password.')
					// TODO: Highlight Password Input/Forgot password? + Display Message
					break
				default:
					alert('Registration failed: ' + errorCode)
					console.log(errorMessage)
			}
		})
		return false
	}

	onPasswordChange(e) {
		this.setState({ pwScore: pwdmeter.chkPass(e.target.value) })
	}

	render() {
		// Redirect to my page if logged in
		if (this.context.currentUser != null)
			return <Redirect push to="my" />
		else return (


<div className="Signup">
	<Header />
	{this.state.isLoading && <Loading />}
	<div className="spacer45"></div>


	<div className="container-fluid" style={{maxWidth:'480px'}}>
		<form onSubmit={ this.onFormSubmit } target="dummyframeSignUp">
			<div className="row">
				<div className="col-12">
					<h3>SIGN UP</h3>
					<hr/>
					{/* Alert */}
					<div className="alert alert-info">
						<strong>1. Your choice on 'YOU ARE' section below</strong>
						{' '}is statistical purpose only. It doesn't cause
						{' '}any difference in using conation service.
						<br/>
						<strong>2. Your email address</strong> will not be exposed to other users.
					</div>

					{/* Who are you? */}
					<small><strong>YOU ARE</strong></small>

					<div className="btn-group btn-group-lg btn-group-toggle w-100" data-toggle="buttons">
						<label className="btn btn-outline-secondary" style={{ width: '33.3333%' }}>
						<input type="radio" name="initial_account_type" id="streamer" /> Streamer
						</label>
						<label className="btn btn-outline-secondary" style={{ width: '33.3333%' }}>
						<input type="radio" name="initial_account_type" id="donator" /> Donator
						</label>
						<label className="btn btn-outline-secondary" style={{ width: '33.3333%' }}>
						<input type="radio" name="initial_account_type" id="just-looking" /> Just Looking
						</label>
					</div>

					{/* Email & Password */}
					<div className="form-group mb-0">
						<div className="spacer30"></div>
						<small><strong>EMAIL AND PASSWORD</strong></small>
						<div className="parent-input-with-icon">
							<i className="far fa-user icon-before-input"></i>
							<input type="email" className="form-control input-after-icon"
								placeholder="Enter email" name="theEmail"/>
						</div>

						<div className="parent-input-with-icon t--1">
							<i className="fas fa-lock icon-before-input"></i>
							<input type="password" className="form-control input-after-icon"
								placeholder="Enter password" name="thePassword"
								onChange={e => this.onPasswordChange(e)} />
							<div className="progress" style={{ height: '2px' }}>
								<div className={"progress-bar" + (this.state.pwScore.score < 40 ? " bg-danger" : " bg-success")}
									role="progressbar" style={{ width: this.state.pwScore.score+'%' }}
									aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>

					</div>

					{/* Terms & Privacy */}
					<div className="spacer30"></div>
					<div className="text-center">
						<p className="text-muted">I agree to the
							<u><NavLink className="text-muted" target="_blank"
								exact to="/policy/privacy"> terms and privacy</NavLink></u>
						</p>
					</div>
					<div className="spacer30"></div>
				</div>
			</div>
			<div className="row">
				<div className="col-12">
					<button type="submit" className="btn btn-lg btn-outline-secondary w-100">Sign up</button>
				</div>
			</div>
		</form>
		<iframe width="0" height="0" name="dummyframeSignUp" className="d-none"></iframe>
	</div>
	<div className="spacer45"></div>
	
	<Footer />
</div>


		)
	}
}
