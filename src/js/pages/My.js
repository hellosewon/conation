import React, { Component } from 'react'
import { Redirect } from 'react-router'
import PropTypes from 'prop-types'



import fire from '../../utils/fire'
import Footer from '../components/Footer'
import Header from '../components/Header'
import Loading from '../components/Loading'
import CardAccountInfo from '../components/My/CardAccountInfo'
import CardStreamerMenu from '../components/My/CardStreamerMenu'



require("firebase/firestore")
export default class My extends Component {

	static contextTypes = {
		currentUser: PropTypes.object,
	}

	constructor(props) {
		super(props)

		this.state = {
			userInfo: undefined,
		}
	}

	getUserInfo() {
		var db = fire.firestore()
		var userRef = db.collection('users').doc(this.context.currentUser.uid)
		userRef.onSnapshot((doc) => {
			if (doc.exists) {
				//alert(JSON.stringify(doc.data()))
				if (this.refs.My) 
					this.setState({userInfo: doc.data()})
			} else {
				if (this.refs.My) 
					this.setState({userInfo: null})
			}
		})
	}

	handleLogoutClick() {
		// Fire Logout
		fire.auth().signOut().then(function() {
			// Sign-out successful.
			
		}).catch(function(error) {
			// An error happened.
			// Handle Errors here.
			var errorCode = error.code
			var errorMessage = error.message
			console.log(errorCode + " " + errorMessage)
			//<Redirect push to="/signin" />
		})
	}

	render_alert1() {
		if (this.state.userInfo === undefined)
			return

		if (this.context.currentUser.emailVerified) {
			// If email_verified and connected_accounts > 0, do not render.
			if (this.state.userInfo)
				if (Object.keys(this.state.userInfo.connected_accounts).length > 0)
					return
			// If email_verfied and no connected_accounts, render with strike.
			return (
				<div className="alert alert-secondary mb-1">
					<s><strong>Step 1.</strong> Verfiy your email address. <u>Learn why.</u></s>
				</div>
			)
		} else {
			// If email is not verified, render.
			return (
				<div className="alert alert-info mb-1">
					<strong>Step 1.</strong> Verfiy your email address. <u>Learn why.</u>
				</div>
			)
		}
	}

	render_alert2() {
		if (this.state.userInfo === undefined)
			return

		if (this.state.userInfo)
			// If connected_accounts > 0, do not render.
			if (Object.keys(this.state.userInfo.connected_accounts).length > 0)
				return
		if (!this.context.currentUser.emailVerified) {
			// If email is not verified
			return (
				<div className="alert alert-secondary">
					<strong>Step 2.</strong> Connect to your existing Twitch and Youtube accounts. <u>Learn the benefits.</u>
				</div>
			)
		} else if (this.context.currentUser.emailVerified) {
			// If email_verified and no connected_accounts, render with info
			return (
				<div className="alert alert-info">
					<strong>Step 2.</strong> Connect to your existing Twitch and Youtube accounts. <u>Learn the benefits.</u>
				</div>
			)
		}
	}

	render_accountInfo() {
		if (!this.state.userInfo)
			return "(Loading account info...)"
		return (<CardAccountInfo userInfo={this.state.userInfo} />)
	}

	render_streamerMenu() {
		if (!this.state.userInfo)
			return "(Loading streamer menu...)"
		return (<CardStreamerMenu userInfo={this.state.userInfo} />)
	}

	render() {

		// Redirect to signin page if not logged in
		if (this.context.currentUser === undefined)
			return <Loading />
		else if (this.context.currentUser === null)
			return <Redirect push to="/signin" />
		else
			return (



<div className="My" ref="My">
	<Header />

	{/* Bound Functions */}
	{this.context.currentUser && !this.state.userInfo && this.getUserInfo()}
	<div className="spacer45"></div>


	<div className="container-fluid" style={{maxWidth:'480px'}}>
		<div className="row">
			<div className="col-12">
				<h3>ACCOUNT</h3>
				<hr/>
				{this.render_alert1()}
				{this.render_alert2()}

				<div className="spacer30"></div>
				{this.render_accountInfo()}

				<div className="spacer45"></div>
				{this.render_streamerMenu()}

				<div className="spacer45"></div>
				{/*
				<button className="btn btn-lg btn-outline-secondary w-100">
				Whitepaper</button>
				<button className="btn btn-lg btn-outline-secondary w-100">
				FAQ</button>
				<button className="btn btn-lg btn-outline-secondary w-100">
				Download App</button>
				*/}
				<button className="btn btn-lg btn-outline-secondary w-100"
					onClick={this.handleLogoutClick}>Logout</button>
			</div>

		</div>

		<div className="spacer45"></div>
	</div>
	<div className="spacer15"></div>
	
	<Footer />
</div>

		)
	}
}
