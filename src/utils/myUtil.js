export const _nwc = (x) => {
	var parts = x.toString().split(".")
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	return parts.join(".")
}

export const _validEmail = (email) => {
	var re = /\S+@\S+\.\S+/
	return email.length > 4 && re.test(email)
}

export const _validPassword = (password) => {
	return password.length > 6
}