import fontawesome from '@fortawesome/fontawesome'


var importedIcons = [
	// Providers
	require('@fortawesome/fontawesome-free-brands/faTwitch'),
	require('@fortawesome/fontawesome-free-brands/faYoutube'),

	// Footer Contacts
	require('@fortawesome/fontawesome-free-brands/faFacebook'),
	require('@fortawesome/fontawesome-free-brands/faTwitter'),
	require('@fortawesome/fontawesome-free-brands/faInstagram'),
	require('@fortawesome/fontawesome-free-brands/faGooglePlusG'),

	// Icons
	require('@fortawesome/fontawesome-free-regular/faUser'),
	require('@fortawesome/fontawesome-free-solid/faUser'),
	require('@fortawesome/fontawesome-free-solid/faLock'),
	require('@fortawesome/fontawesome-free-solid/faHeart'),
	require('@fortawesome/fontawesome-free-solid/faSearch'),

	// Currency
	require('@fortawesome/fontawesome-free-solid/faDollarSign'),
	require('@fortawesome/fontawesome-free-solid/faWonSign'),
	require('@fortawesome/fontawesome-free-brands/faBitcoin'),
	require('@fortawesome/fontawesome-free-brands/faBtc'),
	require('@fortawesome/fontawesome-free-brands/faEthereum'),
	require('@fortawesome/fontawesome-free-brands/faGg'),
]



for (var i = 0; i < importedIcons.length; i++) {
	fontawesome.library.add(importedIcons[i])
}

